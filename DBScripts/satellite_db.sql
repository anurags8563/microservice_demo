USE [master]
GO
/****** Object:  Database [SATELLITE_DB]    Script Date: 4/4/2023 5:47:52 PM ******/
CREATE DATABASE [SATELLITE_DB]
GO
USE [SATELLITE_DB]
GO
/****** Object:  Table [dbo].[References_tb]    Script Date: 4/4/2023 5:47:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[References_tb](
	[ReferenceId] [int] IDENTITY(1,1) NOT NULL,
	[MessageId] [int] NOT NULL,
	[Value] [int] NOT NULL,
	[CreatedDateTime] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ReferenceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Proc_InsertReferenceMessageInfo]    Script Date: 4/4/2023 5:47:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Sp_Proc_InsertReferenceMessageInfo]
(
	@MessageId INT,
	@Value INT
)
AS
BEGIN
	
	INSERT INTO References_tb
	(
		MessageId,
		[Value],
		CreatedDateTime
	)
	VALUES
	(
		@MessageId,
		@Value,
		GETUTCDATE()
	)

	SELECT * FROM References_tb WHERE ReferenceId = SCOPE_IDENTITY()

END
GO
USE [master]
GO
ALTER DATABASE [SATELLITE_DB] SET  READ_WRITE 
GO

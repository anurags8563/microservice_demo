USE [master]
GO
/****** Object:  Database [API_DB]    Script Date: 4/4/2023 5:46:57 PM ******/
CREATE DATABASE [API_DB]
GO
USE [API_DB]
GO
/****** Object:  Table [dbo].[Messages_tb]    Script Date: 4/4/2023 5:46:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Messages_tb](
	[MessageId] [int] IDENTITY(1,1) NOT NULL,
	[Message] [nvarchar](max) NOT NULL,
	[ReferenceId] [int] NULL,
	[ReferenceDateTime] [datetime] NULL,
	[CreatedDateTime] [datetime] NOT NULL,
	[UpdatedDateTime] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[MessageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[Sp_Proc_InsertMessageInfo]    Script Date: 4/4/2023 5:46:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Sp_Proc_InsertMessageInfo]
(
	@Message NVARCHAR(MAX)
)
AS
BEGIN
	
	INSERT INTO Messages_tb
	(
		[Message],
		CreatedDateTime
	)
	VALUES
	(
		@Message,
		GETUTCDATE()
	)

	SELECT * FROM Messages_tb WHERE MessageId = SCOPE_IDENTITY()
END
GO
/****** Object:  StoredProcedure [dbo].[Sp_Proc_UpdateMessageInfo]    Script Date: 4/4/2023 5:46:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Sp_Proc_UpdateMessageInfo]
(
	@MessageId INT,
	@ReferenceId INT,
	@ReferenceDateTime DateTime
)
AS
BEGIN
	
	UPDATE Messages_tb
	SET ReferenceId = @ReferenceId,
	ReferenceDateTime = @ReferenceDateTime,
	UpdatedDateTime = GETUTCDATE()
	WHERE MessageId = @MessageId

END
GO
USE [master]
GO
ALTER DATABASE [API_DB] SET  READ_WRITE 
GO

﻿$(document).ready(function () {

    //this function is used to call the Service API
    $("#btnSubmit").click(function () {
        let jsondata = $('#txtJsonValues').val();

        //Ajax call to Service API and post the requested json data to the API Controller
        $.ajax({
            type: 'post',
            url: 'https://localhost:7078/api/message/save',
            data: JSON.stringify(jsondata),
            contentType: "application/json",
            async: true,
            success: function (data) {
                // On success of API response, clearing the textbox and displaying alert box with success message
                alert("data sent succesfully.");
                $("#txtJsonValues").val('');
            },
            error: function (error) {
                // If any error occurs, displaying alert box with error message
                alert("something went wrong!");
            },
        });
    });
});


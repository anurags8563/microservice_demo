# microservice_demo

## Prerequisites

1- SQL Server
2- .NET 6.0
3- Docker

## Getting started

Steps to run this project

## DB Scripts

Take the database schema from DBScripts folder.

Create two separate databases(API and Satellite) using the scripts.

Once Databases are created in SQL Server, update the appsetting.json of Service API and Satellite API project with your db server name and passwords.

## Rabbit MQ

For service bus use RabbitMQ docker image. Below are the commands that you need to run on your console -

```
docker pull rabbitmq:3-management
docker run --rm -it -p 15672:15672 -p 5672:5672 rabbitmq:3-management
```

Once the rabbitmq image is running, you can login with credentials "guest" as username and password.

## Run the project

Run all the three projects i.e. Microservice_Test_Demo, ServiceAPI & SatelliteAPI. 

## Sample Request JSON
```
Example - 
[
{'id':1,'value':10},
{'id':2,'value':30}
]

Example -
[
{'id':1,'value':50},
{'id':2,'value':70},
{'id':3,'value':90}
]

```
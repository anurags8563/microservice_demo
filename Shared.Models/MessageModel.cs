﻿namespace Shared.Models
{
    //Required models by services
    public class MessageModel
    {
        public int MessageId { get; set; }
        public string Message { get; set; }
        public int? ReferenceId { get; set; }
        public DateTime? ReferenceDateTime { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public DateTime? UpdatedDateTime { get; set; }
        public int SumValue { get; set; }
    }

    public class EventBusSenderRequest
    {
        public int messageid { get; set; }
        public int value { get; set; }
    }

    public class EventBusJsonRequest
    {
        public int id { get; set; }
        public int value { get; set; }
    }
}
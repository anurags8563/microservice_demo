﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Models.Configuration
{
    /// <summary>
    /// AppSetting Configuration Model
    /// </summary>
    public class AppSettings
    {
        public string MessageQueueUri { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Models.Configuration
{
    /// <summary>
    /// Connection Strings Configuration Model
    /// </summary>
    public class ConnectionStrings
    {
        public string DefaultConnection { get; set; }
    }
}

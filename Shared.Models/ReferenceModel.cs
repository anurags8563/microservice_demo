﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared.Models
{
    //Required models by services
    public class ReferenceModel
    {
        public int ReferenceId { get; set; }
        public int MessageId { get; set; }
        public int Value { get; set; }
        public DateTime CreatedDateTime { get; set; }
    }

    public class SatelliteServiceBusRequest
    {
        public int MessageId { get; set; }
        public int ReferenceId { get; set; }
        public DateTime ReferenceDateTime { get; set; }
    }
}

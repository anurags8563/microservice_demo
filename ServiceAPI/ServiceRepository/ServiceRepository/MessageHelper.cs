﻿using ServiceAPI.DataAccess.Interface;
using ServiceAPI.ServiceRepository.InterfaceRepository;
using Shared.Models;

namespace ServiceAPI.ServiceRepository.ServiceRepository
{
    public class MessageHelper : IMessageHelper
    {
        private readonly IMessagesDataRepository _messageRepository;

        /// <summary>
        /// Resolving the required dependencies
        /// </summary>
        /// <param name="messageRepository"></param>
        public MessageHelper(IMessagesDataRepository messageRepository)
        {
            _messageRepository = messageRepository;
        }

        /// <summary>
        /// Add Message Information in API Service
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public MessageModel AddMessageInfo(string message)
        {
            var response = _messageRepository.AddMessageInfo(message);
            return response;
        }

        /// <summary>
        /// Add Message Information in API Service
        /// </summary>
        /// <param name="messageModel"></param>
        public void UpdateMessageInfo(MessageModel messageModel)
        {
            _messageRepository.UpdateMessageInfo(messageModel);
        }
    }
}

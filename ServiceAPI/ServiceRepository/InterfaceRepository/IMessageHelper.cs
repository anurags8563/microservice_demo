﻿using Shared.Models;

namespace ServiceAPI.ServiceRepository.InterfaceRepository
{
    public interface IMessageHelper
    {
        MessageModel AddMessageInfo(string message);

        void UpdateMessageInfo(MessageModel messageModel);
    }
}

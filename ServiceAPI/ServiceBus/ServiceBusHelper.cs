﻿using MassTransit;
using Newtonsoft.Json;
using ServiceAPI.ServiceRepository.InterfaceRepository;
using Shared.Models;

namespace ServiceAPI.ServiceBus
{
    /// <summary>
    /// This class is used to consume the message from RabbitMQ Queue
    /// </summary>
    public class ServiceBusHelper : IConsumer<SatelliteServiceBusRequest>
    {
        private readonly IMessageHelper _messageHelper;

        //resolving the Receiver Helper Service in constructor
        public ServiceBusHelper(IMessageHelper messageHelper)
        {
            _messageHelper = messageHelper;
        }

        /// <summary>
        /// Consume method basically gets invoked whenever there is message queued in service bus
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task Consume(ConsumeContext<SatelliteServiceBusRequest> context)
        {
            try
            {
                //Fetch the message from the context
                var satelldataiteServiceRequest = context.Message;

                if (satelldataiteServiceRequest != null)
                {

                    MessageModel messageModel = new MessageModel()
                    {
                        MessageId = satelldataiteServiceRequest.MessageId,
                        ReferenceId = satelldataiteServiceRequest.ReferenceId,
                        ReferenceDateTime = satelldataiteServiceRequest.ReferenceDateTime
                    };
                    //Update the data into API DB by calling the Message Helper service
                    _messageHelper.UpdateMessageInfo(messageModel);
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}

﻿using Dapper;
using MassTransit.Configuration;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Options;
using ServiceAPI.DataAccess.Interface;
using Shared.Models;
using Shared.Models.Configuration;
using System.Data;

namespace ServiceAPI.DataAccess.Repository
{
    public class MessagesDataRepository : IMessagesDataRepository
    {
        private readonly ConnectionStrings _connectionString;

        /// <summary>
        /// Resolve Connection String
        /// </summary>
        /// <param name="connectionString"></param>
        public MessagesDataRepository(IOptions<ConnectionStrings> connectionString)
        {
            _connectionString = connectionString.Value;
        }

        /// <summary>
        /// Add Message Information into API database
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public MessageModel AddMessageInfo(string message)
        {
            //open database connection
            using var vConn = GetOpenConnection();
            //define parameters needed to be passed in stored procedure
            var vParams = new DynamicParameters();
            vParams.Add("@Message", message);
            //Execute query and get result response from the stored procedure
            var result = vConn.QueryFirstOrDefault<MessageModel>("Sp_Proc_InsertMessageInfo", vParams, commandType: CommandType.StoredProcedure);
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="messageModel"></param>
        public void UpdateMessageInfo(MessageModel messageModel)
        {
            //open database connection
            using var vConn = GetOpenConnection();
            //define parameters needed to be passed in stored procedure
            var vParams = new DynamicParameters();
            vParams.Add("@MessageId", messageModel.MessageId);
            vParams.Add("@ReferenceId", messageModel.ReferenceId);
            vParams.Add("@ReferenceDateTime", messageModel.ReferenceDateTime);
            //Execute query and update result usign the stored procedure
            vConn.Execute("Sp_Proc_UpdateMessageInfo", vParams, commandType: CommandType.StoredProcedure);
        }

        /// <summary>
        /// Estabilishing DB connection using connection string mentioned in appsettings.json
        /// </summary>
        /// <returns></returns>
        public IDbConnection GetOpenConnection()
        {
            return new SqlConnection(_connectionString.DefaultConnection);
        }
    }
}

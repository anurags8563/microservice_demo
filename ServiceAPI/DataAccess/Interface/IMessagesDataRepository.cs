﻿using Shared.Models;
using System.Data;
using static MassTransit.Monitoring.Performance.BuiltInCounters;

namespace ServiceAPI.DataAccess.Interface
{
    public interface IMessagesDataRepository
    {
        IDbConnection GetOpenConnection();

        MessageModel AddMessageInfo(string message);

        void UpdateMessageInfo(MessageModel messageModel);
    }
}

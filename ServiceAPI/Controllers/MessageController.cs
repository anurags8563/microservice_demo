﻿using MassTransit;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using ServiceAPI.ServiceRepository.InterfaceRepository;
using Shared.Models;
using Shared.Models.Configuration;

namespace ServiceAPI.Controllers
{
    [Route("api")]
    [ApiController]
    public class MessageController : ControllerBase
    {
        private readonly IMessageHelper _messageHelper;
        private readonly AppSettings _appSettings;
        private readonly IBus _bus;

        /// <summary>
        /// resolving the MessageHelper, IBus Service in constructor
        /// </summary>
        /// <param name="messageHelper"></param>
        /// <param name="bus"></param>
        /// <param name="appSettings"></param>
        public MessageController(IMessageHelper messageHelper, IBus bus, IOptions<AppSettings> appSettings)
        {
            _messageHelper = messageHelper;
            _bus = bus;
            _appSettings = appSettings.Value;
        }

        /// <summary>
        /// Action method to receive data from App UI
        /// </summary>
        /// <param name="jsondata"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("message/save")]
        public async Task<IActionResult> SaveMessageInfoAsync([FromBody]string? jsondata)
        {
            try
            {
                if (string.IsNullOrEmpty(jsondata))
                {
                    return BadRequest();
                }

                //Add Data in API DB
                var result = _messageHelper.AddMessageInfo(jsondata);
                if (result != null)
                {
                    //get response from API DB after insertion
                    EventBusSenderRequest eventBusSenderRequest = new EventBusSenderRequest() { messageid = result.MessageId }; 
                    var eventBusJsonRequest = JsonConvert.DeserializeObject<List<EventBusJsonRequest>>(result.Message);

                    //calculate the total value from the paased json request
                    foreach(var eventRequest in eventBusJsonRequest)
                    {
                        eventBusSenderRequest.value += eventRequest.value;
                    }

                    //get the message service bus url from appsettings
                    Uri uri = new Uri(_appSettings.MessageQueueUri);
                    //Call RabbitMQ service bus using satellite service reference model
                    var endPoint = await _bus.GetSendEndpoint(uri);
                    await endPoint.Send(eventBusSenderRequest);
                }
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Something went wrong!");
            }
        } 
    }
}

﻿using MassTransit;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using SatelliteAPI.DataAccess.Interface;
using SatelliteAPI.ServiceRepository.InterfaceRepository;
using Shared.Models;
using Shared.Models.Configuration;

namespace SatelliteAPI.ServiceRepository.ServiceRepository
{
    public class ReceiverHelper : IReceiverHelper
    {
        private readonly IReceiverDataRepository _receiverDataRepository;
        private readonly IBus _bus;
        private readonly AppSettings _appSettings;

        //resolving the required dependencies in constructor
        public ReceiverHelper(IReceiverDataRepository receiverDataRepository, IBus bus, IOptions<AppSettings> appSettings)
        {
            _receiverDataRepository = receiverDataRepository;
            _bus = bus;
            _appSettings = appSettings.Value;
        }

        /// <summary>
        /// Method to add received message in Satellite DB 
        /// </summary>
        /// <param name="eventBusSenderRequest"></param>
        /// <returns></returns>
        public ReferenceModel AddReceiverMessageInfo(EventBusSenderRequest eventBusSenderRequest)
        {
            var result = _receiverDataRepository.AddReceiverMessageInfo(eventBusSenderRequest);
            return result;
        }

        /// <summary>
        /// Pass the Satellite Service response to API Service via RabbitMQ service bus
        /// </summary>
        /// <param name="satelliteServiceBusRequest"></param>
        /// <returns></returns>
        public async Task SendReferenceResultToAPI(SatelliteServiceBusRequest satelliteServiceBusRequest)
        {
            //get the message service bus url from appsettings
            Uri uri = new Uri(_appSettings.MessageQueueUri);
            //Call RabbitMQ service bus using satellite service reference model
            var endPoint = await _bus.GetSendEndpoint(uri);
            await endPoint.Send(satelliteServiceBusRequest);
        }
    }
}

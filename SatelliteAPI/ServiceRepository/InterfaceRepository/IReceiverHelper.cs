﻿using Shared.Models;

namespace SatelliteAPI.ServiceRepository.InterfaceRepository
{
    public interface IReceiverHelper
    {
        ReferenceModel AddReceiverMessageInfo(EventBusSenderRequest eventBusSenderRequest);
        Task SendReferenceResultToAPI(SatelliteServiceBusRequest satelliteServiceBusRequest);
    }
}

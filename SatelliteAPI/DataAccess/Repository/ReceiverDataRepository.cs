﻿using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Options;
using SatelliteAPI.DataAccess.Interface;
using Shared.Models;
using Shared.Models.Configuration;
using System.Data;

namespace SatelliteAPI.DataAccess.Repository
{
    public class ReceiverDataRepository : IReceiverDataRepository
    {
        private readonly ConnectionStrings _connectionString;

        //Resolve connection string in constructor
        public ReceiverDataRepository(IOptions<ConnectionStrings> connectionString) 
        {
            _connectionString = connectionString.Value;
        }

        /// <summary>
        /// This method is being used to add receiver message information in Satellite Database 
        /// </summary>
        /// <param name="eventBusSenderRequest"></param>
        /// <returns></returns>
        public ReferenceModel AddReceiverMessageInfo(EventBusSenderRequest eventBusSenderRequest)
        {
            //open database connection
            using var vConn = GetOpenConnection();
            //define parameters needed to be passed in stored procedure
            var vParams = new DynamicParameters();
            vParams.Add("@MessageId", eventBusSenderRequest.messageid);
            vParams.Add("@Value", eventBusSenderRequest.value);
            //Execute query and get result response from the stored procedure
            var result = vConn.QueryFirstOrDefault<ReferenceModel>("Sp_Proc_InsertReferenceMessageInfo", vParams, commandType: CommandType.StoredProcedure);
            return result;
        }

        /// <summary>
        /// Estabilishing DB connection using connection string mentioned in appsettings.json
        /// </summary>
        /// <returns></returns>
        public IDbConnection GetOpenConnection()
        {
            return new SqlConnection(_connectionString.DefaultConnection);
        }
    }
}

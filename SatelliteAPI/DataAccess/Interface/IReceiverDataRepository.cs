﻿using Shared.Models;
using System.Data;

namespace SatelliteAPI.DataAccess.Interface
{
    public interface IReceiverDataRepository
    {
        IDbConnection GetOpenConnection();

        ReferenceModel AddReceiverMessageInfo(EventBusSenderRequest eventBusSenderRequest);
    }
}

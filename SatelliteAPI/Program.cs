using MassTransit;
using Microsoft.OpenApi.Models;
using SatelliteAPI.DataAccess.Interface;
using SatelliteAPI.DataAccess.Repository;
using SatelliteAPI.ServiceBus;
using SatelliteAPI.ServiceRepository.InterfaceRepository;
using SatelliteAPI.ServiceRepository.ServiceRepository;
using Shared.Models.Configuration;

namespace SatelliteAPI
{
    public class Program
    {
        //RabbitMQ uri
        private const string UriString = "rabbitmq://localhost";
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            //Add Mass Transit for service bus config
            builder.Services.AddMassTransit(x =>
            {
                //Add consumer service and bus
                x.AddConsumer<ServiceBusHelper>();
                x.AddBus(provider => Bus.Factory.CreateUsingRabbitMq(config =>
                {
                    config.Host(new Uri(UriString), host =>
                    {
                        host.Username("guest");
                        host.Password("guest");
                    });
                    //Define queue name and consumer
                    config.ReceiveEndpoint("messageQueue", cnf =>
                    {
                        cnf.PrefetchCount = 16;
                        cnf.UseMessageRetry(r => r.Interval(2, 100));
                        cnf.ConfigureConsumer<ServiceBusHelper>(provider);
                    });
                }));
            });



            builder.Services.AddControllers();

            //Cors policies
            builder.Services.AddCors(c =>
            {
                c.AddPolicy("AllowOrigin", options => options.AllowAnyOrigin());
            });

            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Satellite", Version = "v1" });
            });

            //Get AppSetting & ConnectionString section, bind them in model
            var appSettingsSection = builder.Configuration.GetSection("AppSettings");
            builder.Services.Configure<AppSettings>(appSettingsSection);

            var connectionStringSection = builder.Configuration.GetSection("ConnectionStrings");
            builder.Services.Configure<ConnectionStrings>(connectionStringSection);

            // Add services to the container.
            builder.Services.AddTransient<IReceiverHelper, ReceiverHelper>();

            builder.Services.AddTransient<IReceiverDataRepository, ReceiverDataRepository>();

            var app = builder.Build();

            //Cors policies
            app.UseCors(builder => builder
             .AllowAnyHeader()
             .AllowAnyMethod()
             .SetIsOriginAllowed(_ => true)
             .AllowCredentials()
            );

            // Configure the HTTP request pipeline.
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Satellite v1");
            });

            app.UseHttpsRedirection();

            app.UseAuthorization();

            app.MapControllers();

            app.Run();
        }
    }
}
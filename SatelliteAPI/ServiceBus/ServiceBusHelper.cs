﻿using MassTransit;
using Newtonsoft.Json;
using RabbitMQ.Client.Events;
using RabbitMQ.Client;
using SatelliteAPI.ServiceRepository.InterfaceRepository;
using Shared.Models;
using System.Text.Json.Serialization;
using System.Text;

namespace SatelliteAPI.ServiceBus
{
    /// <summary>
    /// This class is used to consume the message from RabbitMQ Queue
    /// </summary>
    public class ServiceBusHelper : IConsumer<EventBusSenderRequest>
    {
        private readonly IReceiverHelper _receiverHelper;

        //resolving the Receiver Helper Service in constructor
        public ServiceBusHelper(IReceiverHelper receiverHelper)
        {
            _receiverHelper = receiverHelper;
        }

        /// <summary>
        /// Consume method basically gets invoked whenever there is message queued in service bus
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task Consume(ConsumeContext<EventBusSenderRequest> context)
        {
            try
            {
                //Fetch the message from the context
                var eventBusSenderRequest = context.Message;

                if (eventBusSenderRequest != null)
                {
                    //Adding the data into satellite DB by calling the Receiver Helper service
                    var referenceModel = _receiverHelper.AddReceiverMessageInfo(eventBusSenderRequest);
                    if (referenceModel != null)
                    {
                        SatelliteServiceBusRequest satelliteServiceBusRequest = new SatelliteServiceBusRequest()
                        {
                            ReferenceId = referenceModel.ReferenceId,
                            MessageId = referenceModel.MessageId,
                            ReferenceDateTime = referenceModel.CreatedDateTime
                        };

                        //Once the data is successfully added in satellite db, we will send the reference id in service bus to API Service project  
                        await _receiverHelper.SendReferenceResultToAPI(satelliteServiceBusRequest);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }


    }
}
